package Database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MetadataHandler {

    /**
     * Get the amount of records in a database table.
     *
     * @param tblName
     * @return
     * @throws SQLException
     */
    public static int getTableCount(String tblName) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        int count = 0;
        ResultSet rs = SQLServer.statement.executeQuery("SELECT COUNT(*) FROM " + tblName);
        while (rs.next()) {
            count = rs.getInt(1);
        }
        return count;
    }
}
