package Functionality;

import Database.SQLServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Basket {

    private ArrayList<Integer> productsList = new ArrayList<>();
    private double totalPrice = 0;

    /**
     * Add new product id to productList
     *
     * @param productId
     */
    public void addProduct(int productId) {
        productsList.add(productId);
    }

    /**
     * Delete product id from productList
     *
     * @param productId
     */
    public void deleteProduct(int productId) {
        for (int id : productsList) {
            if (id == productId) {
                productsList.remove(id);
            }
        }
    }

    /**
     * Calculate the total price of the products in the basket.
     *
     * @return
     * @throws SQLException
     */
    public double calculateTotalPrice() throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        for (int id : productsList) {
            ResultSet rs = SQLServer.statement.executeQuery("SELECT * FROM tblProduct WHERE ProductID = " + id);
            while (rs.next()) {
                totalPrice = totalPrice + rs.getDouble("Price");
            }
        }
        return totalPrice;
    }

    /**
     * Return totalPrice
     *
     * @return
     */
    public double getTotalPrice() {
        return totalPrice;
    }

    /**
     * Delete all products in the basket.
     */
    public void deleteBasket() {
        productsList.clear();
    }

    /**
     * Return produc id's in the productlist
     *
     * @return
     */
    public ArrayList<Integer> getProductsList() {
        return productsList;
    }

    /**
     * Create a new order object and add the product id's.
     *
     * @param userId
     * @throws SQLException
     */
    public void completeOrder(int userId) throws SQLException {
        Order order = new Order();
        order.createOrder(calculateTotalPrice(), userId);
        order.addOrderContent(productsList);
    }
}
