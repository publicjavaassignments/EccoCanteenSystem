package Functionality;

import Database.SQLServer;
import javafx.scene.image.ImageView;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Category extends HandleImage {

    private ImageView imageView;
    private String categoryName;

    /**
     * Select category name from the database
     *
     * @param id
     * @return
     * @throws SQLException
     */
    public String getCategoryName(int id) throws SQLException {
        ResultSet rs = SQLServer.statement.executeQuery("SELECT Name FROM tblCategory WHERE CategoryID = " + id);
        while (rs.next()) {
            categoryName = rs.getString("Name");
        }
        return categoryName;
    }

    /**
     * Set category name
     *
     * @param categoryName
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * Return the imageview which contains the category's image data.
     *
     * @param id
     * @return
     */
    public ImageView getImageView(int id) {
        imageView = new ImageView(getImageData(id, "tblCategory"));
        imageView.setFitHeight(64);
        imageView.setFitWidth(64);
        imageView.setPickOnBounds(true);
        imageView.setPreserveRatio(true);
        return imageView;
    }
}