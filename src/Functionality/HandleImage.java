package Functionality;

import Database.SQLServer;
import javafx.scene.image.Image;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class HandleImage {

    private Image image;

    /**
     * Insert image data into the database
     * Not optimal implementation with if statements to determine which table to insert into, but it works :D
     *
     * @param img
     * @param tblName
     * @param name
     */
    public static void insertImage(String img, String tblName, String name) {
        int imageLength;
        PreparedStatement pstmt;
        String query = "";

        try {
            File file = new File(img);
            FileInputStream stream = new FileInputStream(file);
            imageLength = (int) file.length();
            String fileName = file.getName().substring(0, file.getName().indexOf('.'));

            if (tblName.equals("tblCategory")) {
                query = ("IF NOT EXISTS ( SELECT Name FROM tblCategory WHERE Name = '" + name + "' )\n" +
                        "BEGIN\n" +
                        "    INSERT INTO tblCategory VALUES (?,?,?);\n" +
                        "END\n");
                pstmt = SQLServer.connect.prepareStatement(query);
                pstmt.setString(1, fileName);
                pstmt.setInt(3, imageLength);

                //Insert a stream of bytes
                pstmt.setBinaryStream(2, stream, imageLength);
                pstmt.executeUpdate();
            } else if (tblName.equals("tblProduct")) {
                query = "UPDATE tblProduct SET ImageBlob = ?, ImageLength = ? WHERE Name = '" + name + "';";
                pstmt = SQLServer.connect.prepareStatement(query);

                //Insert a stream of bytes
                pstmt.setBinaryStream(1, stream, imageLength);

                pstmt.setInt(2, imageLength);
                pstmt.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Return selected image from the database.
     * Not optimal implementation with if statements to determine which table to insert into, but it works :D
     * @param id
     * @param tableName
     * @return
     */
    public Image getImageData(int id, String tableName) {
        byte[] fileBytes;
        try {
            SQLServer.statement = SQLServer.connect.createStatement();
            if (tableName.equals("tblCategory")) {
                ResultSet rs = SQLServer.statement.executeQuery("SELECT ImageBlob FROM " + tableName + " WHERE CategoryID = " + id);
                while (rs.next()) {
                    fileBytes = rs.getBytes("ImageBlob");

                    //Create temporary file to store the image
                    File tempFile = File.createTempFile("image", ".png");

                    OutputStream targetFile = new FileOutputStream(tempFile);
                    targetFile.write(fileBytes);
                    image = new Image("File:///" + tempFile.getAbsolutePath());
                }
            } else if (tableName.equals("tblProduct")) {
                ResultSet rs = SQLServer.statement.executeQuery("SELECT ImageBlob FROM tblProduct WHERE ProductID = " + id);
                while (rs.next()) {
                    fileBytes = rs.getBytes("ImageBlob");

                    //Create temporary file to store the image
                    File tempFile = File.createTempFile("image", ".png");

                    OutputStream targetFile = new FileOutputStream(tempFile);
                    targetFile.write(fileBytes);
                    image = new Image("File:///" + tempFile.getAbsolutePath());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }
}