package Functionality;

import Database.SQLServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Order {
    private int orderId;

    /**
     * Insert Order metadata to the database;
     *
     * @param totalPrice
     * @param userId
     * @throws SQLException
     */
    public void createOrder(double totalPrice, int userId) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String dateStr = formatter.format(date);

        SQLServer.statement.executeUpdate("INSERT INTO tblOrder (UserID, Date, Price, OrderFinished) " +
                "VALUES (" + userId + ", '" + dateStr + "', '" + totalPrice + "', 1)");
    }

    /**
     * Return the highest order id to get the most recent order.
     *
     * @return
     * @throws SQLException
     */
    public int getOrderId() throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        ResultSet rs = SQLServer.statement.executeQuery("SELECT TOP 1 * FROM tblOrder ORDER BY OrderID DESC");
        while (rs.next()) {
            orderId = rs.getInt("OrderID");
        }
        return orderId;
    }

    /**
     * Add the products in the order, to the database
     *
     * @param productIdList
     * @throws SQLException
     */
    public void addOrderContent(ArrayList<Integer> productIdList) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        String date = "";

        for (int id : productIdList) {
            ResultSet rs = SQLServer.statement.executeQuery("SELECT Date FROM tblOrder WHERE OrderID = " + getOrderId());
            while (rs.next()) {
                date = rs.getString("Date");
            }

            SQLServer.statement.executeUpdate("INSERT INTO tblOrderContent (OrderID, ProductID, Date)" +
                    "VALUES (" + getOrderId() + ", " + id + ", '" + date + "');");
        }
    }
}
