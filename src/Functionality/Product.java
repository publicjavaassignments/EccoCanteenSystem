package Functionality;

import Database.SQLServer;
import javafx.scene.image.Image;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Product extends HandleImage {

    private String productNameStr;
    private String productPriceStr;
    private Image image;
    private int quantity;
    private int categoryId;
    private int supplierId;
    private int minQuantity;

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public String getProductNameStr() {
        return productNameStr;
    }

    public void setProductNameStr(String productNameStr) {
        this.productNameStr = productNameStr;
    }

    public String getProductPriceStr() {
        return productPriceStr;
    }

    public void setProductPriceStr(String productPriceStr) {
        this.productPriceStr = productPriceStr;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getMinQuantity() {
        return minQuantity;
    }

    public void setMinQuantity(int minQuantity) {
        this.minQuantity = minQuantity;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * Select the product details from the database.
     *
     * @param id
     * @throws SQLException
     */
    public void getProductDetails(int id) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        ResultSet rs = SQLServer.statement.executeQuery("SELECT * FROM tblProduct WHERE ProductID = " + id);
        while (rs.next()) {
            productNameStr = rs.getString("Name");
            productPriceStr = rs.getString("Price");
            quantity = rs.getInt("Quantity");
            minQuantity = rs.getInt("MinQuantity");
            categoryId = rs.getInt("CategoryID");
            supplierId = rs.getInt("SupplierID");
        }
        image = getImageData(id, "tblProduct");
    }

    /**
     * Create a new product with an image.
     *
     * @param name
     * @param quantity
     * @param categoryId
     * @param price
     * @param imgPath
     * @param supplierId
     * @param minQuantity
     * @throws SQLException
     */
    public static void insertProduct(String name, int quantity, int categoryId, double price, String imgPath, int supplierId, int minQuantity) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        SQLServer.statement.executeUpdate("INSERT INTO tblProduct (Name, Quantity, CategoryID, Price, SupplierID, MinQuantity)" +
                "VALUES ('" + name + "', " + quantity + ", " + categoryId + ", " + price + ", " + supplierId + ", " + minQuantity + ");");

        insertImage(imgPath, "tblProduct", name);
    }

    /**
     * Create a new product without an image.
     *
     * @param name
     * @param quantity
     * @param categoryId
     * @param price
     * @param supplierId
     * @param minQuantity
     * @throws SQLException
     */
    public static void insertProduct(String name, int quantity, int categoryId, double price, int supplierId, int minQuantity) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        SQLServer.statement.executeUpdate("INSERT INTO tblProduct (Name, Quantity, CategoryID, Price, SupplierID, MinQuantity)" +
                "VALUES ('" + name + "', " + quantity + ", " + categoryId + ", " + price + ", " + supplierId + ", " + minQuantity + ");");

    }

    /**
     * Select all the product in the selected category.
     *
     * @param category
     * @return
     * @throws SQLException
     */
    public static ArrayList<Integer> getProductId(String category) throws SQLException {
        ArrayList<Integer> productIdList = new ArrayList<>();
        SQLServer.statement = SQLServer.connect.createStatement();
        ResultSet rs = SQLServer.statement.executeQuery("SELECT ProductID FROM tblProduct " +
                "WHERE CategoryID = (SELECT CategoryID FROM tblCategory WHERE Name = '" + category + "');");
        while (rs.next()) {
            productIdList.add(rs.getInt("ProductID"));
        }
        return productIdList;
    }

    /**
     * Update the stock when a product has been sold.
     *
     * @param productId
     * @throws SQLException
     */
    public void updateInventory(int productId) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        quantity--;
        SQLServer.statement.executeUpdate("UPDATE tblProduct SET Quantity = " + quantity + "WHERE ProductID = " + productId + ";");

    }
}