package Functionality;

import Database.SQLServer;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Supplier {

    private int supplierId;
    private String supplierName;
    private String supplierPhone;
    private String supplierEmail;
    private String supplierAddress;

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierPhone() {
        return supplierPhone;
    }

    public void setSupplierPhone(String supplierPhone) {
        this.supplierPhone = supplierPhone;
    }

    public String getSupplierEmail() {
        return supplierEmail;
    }

    public void setSupplierEmail(String supplierEmail) {
        this.supplierEmail = supplierEmail;
    }

    public String getSupplierAddress() {
        return supplierAddress;
    }

    public void setSupplierAddress(String supplierAddress) {
        this.supplierAddress = supplierAddress;
    }

    /**
     * Insert new supplier to the database.
     *
     * @param supplierName
     * @param supplierPhone
     * @param supplierEmail
     * @param supplierAddress
     * @throws SQLException
     */
    public void insertSupplier(String supplierName, String supplierPhone, String supplierEmail, String supplierAddress) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();

        SQLServer.statement.executeUpdate("INSERT INTO tblSupplier (Name, PhoneNumber, Email, Address)" +
                "VALUES ('" + supplierName + "', '" + supplierPhone + "', '" + supplierEmail + "', '" + supplierAddress + "');");

    }

    /**
     * Return the selected product's supplier name
     *
     * @param productId
     * @return
     * @throws SQLException
     */
    public String getName(int productId) throws SQLException {
        String supplierName = "";
        SQLServer.statement = SQLServer.connect.createStatement();

        ResultSet rs = SQLServer.statement.executeQuery("SELECT Name FROM tblSupplier " +
                "WHERE SupplierID = (SELECT SupplierID FROM tblProduct WHERE ProductID = " + productId + "); ");
        while (rs.next()) {
            supplierName = rs.getString("Name");
        }
        return supplierName;
    }

    /**
     * Return all data from the selected supplier
     *
     * @param name
     * @throws SQLException
     */
    public void getSupplierInfo(String name) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();

        ResultSet rs = SQLServer.statement.executeQuery("SELECT * FROM tblSupplier " +
                "WHERE SupplierID = (SELECT SupplierID FROM tblSupplier WHERE Name = '" + name + "');");

        while (rs.next()) {
            supplierId = rs.getInt("SupplierID");
            supplierName = rs.getString("Name");
            supplierPhone = rs.getString("PhoneNumber");
            supplierEmail = rs.getString("Email");
            supplierAddress = rs.getString("Address");
        }
    }

    /**
     * Get the supplier's contact info and update the stock.
     * Since there's no real supplier to contact, the stock just updates with the decided quantity
     *
     * @param productId
     * @param quantity
     * @throws SQLException
     */
    public void orderNewProduct(int productId, int quantity) throws SQLException {

        SQLServer.statement = SQLServer.connect.createStatement();
        int supplierId = 0;
        String supplierEmail = "";

        ResultSet rs = SQLServer.statement.executeQuery("SELECT SupplierID FROM tblProduct WHERE ProductID = " + productId);
        while (rs.next()) {
            supplierId = rs.getInt("SupplierID");
        }

        rs = SQLServer.statement.executeQuery("SELECT Email FROM tblSupplier WHERE SupplierID = " + supplierId + ";");
        while (rs.next()) {
            supplierEmail = rs.getString("Email");
        }

        /*

        Setup automatic contact to supplier to order new products.

         */

        SQLServer.statement.executeUpdate("UPDATE tblProduct SET Quantity = " + quantity + " WHERE ProductID = " + productId + ";");

    }

}
