package GUI;

import Database.SQLServer;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;

public class BackendController {

    @FXML
    private JFXTreeTableView<BackendProduct> orderView;
    @FXML
    private JFXTreeTableView<BackendProduct> productTable;
    @FXML
    private JFXTreeTableColumn<BackendProduct, String> colProductName, colProductCategory, colProductSupplier, colProductId, colProductPrice, colProductQuantity;
    @FXML
    private JFXTreeTableView<BackendSupplier> supplierTable;
    @FXML
    private JFXTreeTableColumn<BackendSupplier, String> colSupplierId, colSupplierName, colSupplierPhone, colSupplierEmail, colSupplierAddress;
    @FXML
    private JFXListView orderListView, categoryListView, supplierListView, orderListViewChart;
    @FXML
    private LineChart lineChart;

    private ObservableList<String> categoryList = FXCollections.observableArrayList();
    private ObservableList<BackendProduct> productList = FXCollections.observableArrayList();
    private ObservableList<String> supplierList = FXCollections.observableArrayList();
    private ObservableList<BackendSupplier> supplierInfoList = FXCollections.observableArrayList();

    private static Stage newProductStage = new Stage();
    private static Stage newSupplierStage = new Stage();


    public static Stage getNewProductStage() {
        return newProductStage;
    }

    public static Stage getNewSupplierStage() {
        return newSupplierStage;
    }

    public void initialize() throws SQLException, IOException {

        populateOrderListView();
        populateCategoryList();
        populateSupplierList();
        loadNewProductFxml();
        loadNewSupplierFxml();
        populateStatisticsProducts();


    }

    /**
     * Select all orders and populate order ListView.
     *
     * @throws SQLException
     */
    public void populateOrderListView() throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        ResultSet rs;
        try {
            rs = SQLServer.statement.executeQuery("SELECT * FROM tblOrder");

            while (rs.next()) {
                String orderNumber = rs.getString(1);
                String price = rs.getString(4);
                int orderStatus = rs.getInt(5);

                String formattedPrice = "Pris: " + price.replace(".", ",") + " dkkr";

                StackPane root = new StackPane();

                HBox hbox = new HBox();
                VBox vbox = new VBox();

                JFXCheckBox checkbox = new JFXCheckBox();

                if (orderStatus == 1) {
                    checkbox.setSelected(true);
                }

                EventHandler<ActionEvent> event = e -> {
                    if (checkbox.isSelected()) {
                        try {
                            String orderNumberRaw = vbox.getChildren().get(0).toString();
                            String orderNumberTreated = StringUtils.substringBetween(orderNumberRaw, "(", ")");
                            SQLServer.statement.executeUpdate("UPDATE tblOrder SET OrderFinished = '1' where OrderID = " + orderNumberTreated);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                    }
                    if (!checkbox.isSelected()) {
                        {
                            try {
                                String orderNumberRaw = vbox.getChildren().get(0).toString();
                                String orderNumberTreated = StringUtils.substringBetween(orderNumberRaw, "(", ")");
                                SQLServer.statement.executeUpdate("UPDATE tblOrder SET OrderFinished = '0' where OrderID = " + orderNumberTreated);
                            } catch (SQLException ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                };

                EventHandler<MouseEvent> getID = e -> {
                    String orderNumberRaw = vbox.getChildren().get(0).toString();
                    String orderNumberTreated = StringUtils.substringBetween(orderNumberRaw, "(", ")");
                    populateOrder(orderNumberTreated);
                };

                root.setOnMouseClicked(getID);
                checkbox.setOnAction(event);

                Label orderNumberLbl = new Label("Order number: (" + orderNumber + ")");
                Label orderPriceLbl = new Label(formattedPrice);

                vbox.getChildren().addAll(orderNumberLbl, orderPriceLbl);
                hbox.getChildren().addAll(checkbox, vbox);
                root.getChildren().addAll(hbox);

                orderListView.getItems().add(root);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     * Create a table with information about the selected order.
     *
     * @param orderID
     */
    public void populateOrder(String orderID) {
        ObservableList<BackendProduct> products = FXCollections.observableArrayList();

        // Set up columns
        JFXTreeTableColumn<BackendProduct, String> OrderID = new JFXTreeTableColumn<>("Product #");
        OrderID.setPrefWidth(75);
        OrderID.setCellValueFactory(param -> param.getValue().getValue().productID);
        JFXTreeTableColumn<BackendProduct, String> Name = new JFXTreeTableColumn<>("Name");
        Name.setPrefWidth(200);
        Name.setCellValueFactory(param -> param.getValue().getValue().name);
        JFXTreeTableColumn<BackendProduct, String> Price = new JFXTreeTableColumn<>("Price");
        Price.setPrefWidth(125);
        Price.setCellValueFactory(param -> param.getValue().getValue().price);


        try {
            SQLServer.statement = SQLServer.connect.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ResultSet rs;
        try {
            rs = SQLServer.statement.executeQuery("SELECT * FROM tblProduct WHERE ProductID IN (SELECT ProductId FROM tblOrderContent WHERE OrderID = " + orderID + ")");

            while (rs.next()) {
                String orderNumber = rs.getString(1);
                String NameString = rs.getString(2);
                String PriceString = rs.getString(5);

                String formattedPrice = PriceString.replace(".", ",") + " dkkr";

                products.add(new BackendProduct(orderNumber, NameString, formattedPrice));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        final TreeItem<BackendProduct> root = new RecursiveTreeItem<>(products, RecursiveTreeObject::getChildren);
        orderView.getColumns().setAll(Name, Price, OrderID);
        orderView.setRoot(root);
        orderView.setShowRoot(false);
    }


    /**
     * Select all the category names from the database, and populate the category listview.
     *
     * @throws SQLException
     */
    public void populateCategoryList() throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();

        ResultSet rs = SQLServer.statement.executeQuery("SELECT Name FROM tblCategory");
        while (rs.next()) {
            categoryList.add(rs.getString("Name"));
        }
        categoryListView.setItems(categoryList);
        categoryListView.setOnMouseClicked(event -> {
            String category = categoryListView.getSelectionModel().getSelectedItems().toString();
            category = category.substring(1, category.length() - 1);

            try {
                getCategoryProducts(category);
            } catch (SQLException e) {
                e.printStackTrace();
            }

        });
    }

    /**
     * Select all product name and populate product statistics listview.
     *
     * @throws SQLException
     */
    public void populateStatisticsProducts() throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();

        ResultSet rs;
        try {
            rs = SQLServer.statement.executeQuery("SELECT Name FROM tblProduct");

            while (rs.next()) {
                String name = rs.getString(1);

                StackPane root = new StackPane();

                EventHandler<MouseEvent> generateChart = e -> {
                    String nameStringRaw = root.getChildren().get(0).toString();
                    String nameStringTreated = StringUtils.substringBetween(nameStringRaw, "'", "'");
                    generateChart(nameStringTreated);
                };

                root.setOnMouseClicked(generateChart);
                Label productName = new Label(name);
                root.getChildren().addAll(productName);
                orderListViewChart.getItems().add(root);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Generate statistics chart, that shows daily sales of each product.
     *
     * @param name
     */
    public void generateChart(String name) {
        XYChart.Series series = new XYChart.Series();
        series.setName(name);
        series.getData().removeAll(Collections.singleton(lineChart.getData().setAll()));

        try {
            SQLServer.statement = SQLServer.connect.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ResultSet rs;
        try {
            rs = SQLServer.statement.executeQuery("SELECT COUNT(ProductID),Date FROM tblOrderContent WHERE ProductID = (SELECT ProductID FROM tblProduct WHERE Name = '" + name + "') GROUP BY Date");

            while (rs.next()) {
                int count = rs.getInt(1);
                String date = rs.getString(2);

                series.getData().add(new XYChart.Data(date, count));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        lineChart.getData().add(series);
    }

    /**
     * Load the FXML for creating a new product.
     *
     * @throws IOException
     */
    public void loadNewProductFxml() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("NewProduct.fxml"));
        Parent root = fxmlLoader.load();
        JFXDecorator decorator = new JFXDecorator(newProductStage, root);
        decorator.setCustomMaximize(false);
        Scene scene = new Scene(decorator);
        newProductStage.setScene(scene);
    }

    /**
     * Load the FXML for creating a new supplier.
     *
     * @throws IOException
     */
    public void loadNewSupplierFxml() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("NewSupplier.fxml"));
        Parent root = fxmlLoader.load();
        JFXDecorator decorator = new JFXDecorator(newSupplierStage, root);
        decorator.setCustomMaximize(false);
        Scene scene = new Scene(decorator);
        newSupplierStage.setScene(scene);
    }

    /**
     * Handle new product button click, and show the stage.
     */
    public void handleNewProduct() {
        newProductStage.setTitle("Admin Panel");
        newProductStage.show();
    }

    /**
     * Handle new supplier button click, and show the stage.
     */
    public void handleNewSupplier() {
        newSupplierStage.setTitle("New supplier");
        newSupplierStage.show();
    }

    /**
     * Select all product info from the selected category, and add the new BackendProduct to the product list
     *
     * @param category
     * @throws SQLException
     */
    public void getCategoryProducts(String category) throws SQLException {
        productList.clear();
        Statement st1 = SQLServer.connect.createStatement();
        Statement st2 = SQLServer.connect.createStatement();
        ResultSet rs1 = st1.executeQuery("SELECT * FROM tblProduct WHERE CategoryID = (SELECT CategoryID FROM tblCategory WHERE Name = '" + category + "');");
        ResultSet rs2;

        while (rs1.next()) {
            String productId = rs1.getString("ProductID");
            String productName = rs1.getString("Name");
            String price = rs1.getString("Price");
            String quantity = rs1.getString("Quantity");
            int supplierId = rs1.getInt("SupplierID");
            String supplierName = "";

            rs2 = st2.executeQuery("SELECT Name FROM tblSupplier WHERE SupplierID = " + supplierId);
            while (rs2.next()) {
                supplierName = rs2.getString("Name");
            }

            productList.add(new BackendProduct(productId, productName, price, category, quantity, supplierName));
        }

        colProductName.setPrefWidth(150);
        colProductId.setCellValueFactory(param -> param.getValue().getValue().productID);
        colProductName.setCellValueFactory(param -> param.getValue().getValue().name);
        colProductQuantity.setCellValueFactory(param -> param.getValue().getValue().quantity);
        colProductPrice.setCellValueFactory(param -> param.getValue().getValue().price);
        colProductCategory.setCellValueFactory(param -> param.getValue().getValue().category);
        colProductSupplier.setCellValueFactory(param -> param.getValue().getValue().supplier);

        final TreeItem<BackendProduct> root = new RecursiveTreeItem<>(productList, RecursiveTreeObject::getChildren);
        productTable.getColumns().setAll(colProductId, colProductName, colProductQuantity, colProductPrice, colProductCategory, colProductSupplier);
        productTable.setRoot(root);
        productTable.setShowRoot(false);
    }

    /**
     * Select all supplier names and populate the supplier listview.
     *
     * @throws SQLException
     */
    public void populateSupplierList() throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();

        ResultSet rs = SQLServer.statement.executeQuery("SELECT Name FROM tblSupplier");
        while (rs.next()) {
            supplierList.add(rs.getString("Name"));
        }
        supplierListView.setItems(supplierList);
        supplierListView.setOnMouseClicked(event -> {
            String supplier = supplierListView.getSelectionModel().getSelectedItems().toString();
            supplier = supplier.substring(1, supplier.length() - 1);
            try {
                getSupplierInfo(supplier);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Select all data about the selected supplier, and populate the supplierInfoList
     *
     * @param supplier
     * @throws SQLException
     */
    public void getSupplierInfo(String supplier) throws SQLException {

        supplierInfoList.clear();
        SQLServer.statement = SQLServer.connect.createStatement();

        ResultSet rs = SQLServer.statement.executeQuery("SELECT * FROM tblSupplier WHERE Name = '" + supplier + "';");
        while (rs.next()) {
            String supplierId = rs.getString("SupplierId");
            String supplierName = rs.getString("Name");
            String supplierPhone = rs.getString("PhoneNumber");
            String supplierEmail = rs.getString("Email");
            String supplierAddress = rs.getString("Address");
            supplierInfoList.add(new BackendSupplier(supplierId, supplierName, supplierPhone, supplierEmail, supplierAddress));
        }

        colSupplierId.setCellValueFactory(param -> param.getValue().getValue().supplierId);
        colSupplierName.setCellValueFactory(param -> param.getValue().getValue().supplierName);
        colSupplierPhone.setCellValueFactory(param -> param.getValue().getValue().supplierPhone);
        colSupplierEmail.setCellValueFactory(param -> param.getValue().getValue().supplierEmail);
        colSupplierAddress.setCellValueFactory(param -> param.getValue().getValue().supplierAddress);

        final TreeItem<BackendSupplier> root = new RecursiveTreeItem<>(supplierInfoList, RecursiveTreeObject::getChildren);
        supplierTable.getColumns().setAll(colSupplierId, colSupplierName, colSupplierPhone, colSupplierEmail, colSupplierAddress);
        supplierTable.setRoot(root);
        supplierTable.setShowRoot(false);
    }
}