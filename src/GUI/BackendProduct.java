package GUI;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BackendProduct extends RecursiveTreeObject<BackendProduct> {
    StringProperty productID;
    StringProperty name;
    StringProperty price;
    StringProperty category;
    StringProperty quantity;
    StringProperty supplier;

    /**
     * Constructor to create a new BackendProduct with a category and a supplier, which is used for displaying products in the admin panel.
     *
     * @param productId
     * @param name
     * @param price
     * @param category
     * @param quantity
     * @param supplier
     */
    public BackendProduct(String productId, String name, String price, String category, String quantity, String supplier) {
        this.productID = new SimpleStringProperty(productId);
        this.name = new SimpleStringProperty(name);
        this.price = new SimpleStringProperty(price);
        this.category = new SimpleStringProperty(category);
        this.quantity = new SimpleStringProperty(quantity);
        this.supplier = new SimpleStringProperty(supplier);
    }

    /**
     * Constructor to create a new BackendProduct without a category and a supplier, which is used for displaying products in the admin panel.
     *
     * @param productId
     * @param name
     * @param price
     */
    public BackendProduct(String productId, String name, String price) {
        this.productID = new SimpleStringProperty(productId);
        this.name = new SimpleStringProperty(name);
        this.price = new SimpleStringProperty(price);
    }
}