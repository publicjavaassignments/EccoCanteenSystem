package GUI;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BackendSupplier extends RecursiveTreeObject<BackendSupplier> {

    StringProperty supplierId;
    StringProperty supplierName;
    StringProperty supplierPhone;
    StringProperty supplierEmail;
    StringProperty supplierAddress;

    /**
     * Constructor to create a new BackendSupplier object, which is used for displaying suppliers in the admin panel.
     *
     * @param supplierId
     * @param supplierName
     * @param supplierPhone
     * @param supplierEmail
     * @param supplierAddress
     */
    public BackendSupplier(String supplierId, String supplierName, String supplierPhone, String supplierEmail, String supplierAddress) {
        this.supplierId = new SimpleStringProperty(supplierId);
        this.supplierName = new SimpleStringProperty(supplierName);
        this.supplierPhone = new SimpleStringProperty(supplierPhone);
        this.supplierEmail = new SimpleStringProperty(supplierEmail);
        this.supplierAddress = new SimpleStringProperty(supplierAddress);
    }
}
