package GUI;

import Database.SQLServer;
import Functionality.Basket;
import Functionality.Category;
import Functionality.Product;
import Functionality.Supplier;
import com.jfoenix.controls.*;
import com.jfoenix.effects.JFXDepthManager;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static javafx.animation.Interpolator.EASE_BOTH;

public class FrontendController {
    @FXML
    private ScrollPane scrollPane, categoryListScrollPane;
    @FXML
    private JFXMasonryPane masonryPane;
    @FXML
    private VBox menuVBox;
    @FXML
    private BorderPane PaneRoot;
    @FXML
    private StackPane root;
    @FXML
    private JFXButton BasketButton;

    private static final String ROBOTO = "Roboto";
    private Basket basket = new Basket();
    private int basketCounter = 0;

    private double width = 200;
    private double height = 200;

    private ArrayList<Node> children = new ArrayList<>();
    private Stage adminStage = new Stage();

    public void initialize() throws SQLException, IOException {
        populateCategoryMenu();
        addAdminButton();
        createProduct("Bread");
        loadAdminFxml();

        categoryListScrollPane.setScaleX(0);

        Timeline categoryAnimation = new Timeline(new KeyFrame(Duration.millis(400),
                new KeyValue(categoryListScrollPane.scaleXProperty(),
                        1,
                        EASE_BOTH)));
        categoryAnimation.setDelay(Duration.millis(100 + 1000));
        categoryAnimation.play();

        JFXScrollPane.smoothScrolling(categoryListScrollPane);
        JFXScrollPane.smoothScrolling(scrollPane);
    }


    /**
     * Sets the active category, and calls createProduct to create all products in the selected category.
     *
     * @param activeCategory
     * @throws SQLException
     */
    public void setActiveCategory(String activeCategory) throws SQLException {
        masonryPane.getChildren().removeAll(children);
        children.clear();
        createProduct(activeCategory);
    }


    /**
     * Create all the products in the selected category.
     *
     * @param activeCategory
     * @return
     * @throws SQLException
     */
    public ArrayList<Node> createProduct(String activeCategory) throws SQLException {

        //Create a new product for each product in the category
        for (int productId : Product.getProductId(activeCategory)) {
            Product product = new Product();
            product.getProductDetails(productId);

            StackPane child = new StackPane();
            child.setPrefWidth(width);
            child.setPrefHeight(height);
            JFXDepthManager.setDepth(child, 1);
            child.setScaleX(0);
            child.setScaleY(0);
            children.add(child);

            // Create card
            StackPane header = new StackPane();
            JFXButton headerButton = new JFXButton("");
            headerButton.setMinHeight(160);
            headerButton.setMinWidth(230);

            //Create product imageview
            ImageView productImageView = new ImageView(product.getImage());
            productImageView.setPreserveRatio(false);
            productImageView.setFitHeight(160);
            productImageView.setFitWidth(230);
            header.getChildren().addAll(productImageView, headerButton);

            //Select random color for the product, in case there is not an image.
            String headerColor = getRandomColor(children.size() % 12);
            header.setStyle("-fx-background-radius: 5 5 0 0; -fx-background-color: " + headerColor);
            VBox.setVgrow(header, Priority.ALWAYS);
            StackPane body = new StackPane();
            VBox cardLabels = new VBox();
            cardLabels.setAlignment(Pos.CENTER_LEFT);

            body.setMinHeight(70);
            VBox content = new VBox();

            // Labels
            Label productNameLbl = new Label(" " + product.getProductNameStr());
            productNameLbl.setTextFill(Paint.valueOf("#646464"));
            productNameLbl.setFont(Font.font(ROBOTO, FontWeight.NORMAL, 16));
            productNameLbl.setScaleX(0);
            productNameLbl.setScaleY(0);

            String formattedPrice = " " + product.getProductPriceStr().replace(".", ",") + " dkkr";
            Label productPriceLbl = new Label(formattedPrice);
            productPriceLbl.setTextFill(Paint.valueOf("#646464"));
            productPriceLbl.setFont(Font.font(ROBOTO, FontWeight.BOLD, 16));
            productPriceLbl.setScaleX(0);
            productPriceLbl.setScaleY(0);

            content.getChildren().addAll(header, body);
            body.getChildren().addAll(cardLabels);
            cardLabels.getChildren().addAll(productNameLbl);
            cardLabels.getChildren().addAll(productPriceLbl);
            body.setStyle("-fx-background-radius: 0 0 5 5; -fx-background-color: rgb(255,255,255,0.87);");

            // Create buy button
            JFXButton buyButton = new JFXButton("");
            buyButton.setButtonType(JFXButton.ButtonType.RAISED);
            buyButton.setStyle("-fx-background-radius: 40;-fx-background-color: #323232");
            buyButton.setPrefSize(40, 40);
            buyButton.setRipplerFill(Paint.valueOf("#ffffff"));
            buyButton.setScaleX(0);
            buyButton.setScaleY(0);
            FontAwesomeIconView glyph = new FontAwesomeIconView(FontAwesomeIcon.SHOPPING_BASKET);
            buyButton.setGraphic(glyph);
            glyph.setStyle("-fx-font-size: 24px;");
            glyph.setFill(Paint.valueOf("#ffffff"));

            buyButton.translateYProperty().bind(Bindings.createDoubleBinding(() ->
                            header.getBoundsInParent().getHeight() - buyButton.getHeight() / 2,
                    header.boundsInParentProperty(), buyButton.heightProperty()));

            StackPane.setMargin(buyButton, new Insets(0, 12, 0, 0));
            StackPane.setAlignment(buyButton, Pos.TOP_RIGHT);

            //Add product to the basket.
            buyButton.setOnAction(event -> {
                basket.addProduct(productId);
                basketCounter++;
                BasketButton.setText("Basket (" + basketCounter + ")");

            });

            // Timeline animations to display the products
            Timeline cardAnimation = new Timeline(new KeyFrame(Duration.millis(200),
                    new KeyValue(child.scaleXProperty(),
                            1,
                            EASE_BOTH),
                    new KeyValue(child.scaleYProperty(),
                            1,
                            EASE_BOTH)));
            cardAnimation.setDelay(Duration.millis(100 * children.size() + 1000));
            Timeline cardContent = new Timeline(new KeyFrame(Duration.millis(240),
                    new KeyValue(productPriceLbl.scaleXProperty(),
                            1,
                            EASE_BOTH),
                    new KeyValue(productPriceLbl.scaleYProperty(),
                            1,
                            EASE_BOTH),
                    new KeyValue(productNameLbl.scaleXProperty(),
                            1,
                            EASE_BOTH),
                    new KeyValue(productNameLbl.scaleYProperty(),
                            1,
                            EASE_BOTH),
                    new KeyValue(buyButton.scaleXProperty(),
                            1,
                            EASE_BOTH),
                    new KeyValue(buyButton.scaleYProperty(),
                            1,
                            EASE_BOTH)));
            cardContent.setDelay(Duration.millis(150 * children.size() + 1000));

            // Play animations
            cardContent.play();
            cardAnimation.play();

            child.getChildren().addAll(content, buyButton);
        }

        masonryPane.getChildren().addAll(children);
        Platform.runLater(() -> scrollPane.requestLayout());

        return children;
    }

    /**
     * Return a random color
     *
     * @param i
     * @return
     */
    private String getRandomColor(int i) {
        String color = "#FFFFFF";
        switch (i) {
            case 0:
                color = "#8F3F7E";
                break;
            case 1:
                color = "#B5305F";
                break;
            case 2:
                color = "#CE584A";
                break;
            case 3:
                color = "#DB8D5C";
                break;
            case 4:
                color = "#DA854E";
                break;
            case 5:
                color = "#E9AB44";
                break;
            case 6:
                color = "#FEE435";
                break;
            case 7:
                color = "#99C286";
                break;
            case 8:
                color = "#01A05E";
                break;
            case 9:
                color = "#4A8895";
                break;
            case 10:
                color = "#16669B";
                break;
            case 11:
                color = "#2F65A5";
                break;
            case 12:
                color = "#4E6A9C";
                break;
            default:
                break;
        }
        return color;
    }

    /**
     * Create the category menu GUI.
     *
     * @param category
     * @param id
     * @return
     * @throws SQLException
     */
    public StackPane createCategoryMenu(Category category, int id) throws SQLException {
        StackPane container = new StackPane();
        ImageView imgView = category.getImageView(id);
        JFXButton button = new JFXButton();
        button.prefHeightProperty().bind(imgView.fitHeightProperty());
        button.prefWidthProperty().bind(imgView.fitWidthProperty());
        Tooltip tooltip = new Tooltip(category.getCategoryName(id));
        button.setTooltip(tooltip);

        // set active category to the selected category.
        button.setOnAction(event -> {
            try {
                setActiveCategory(category.getCategoryName(id));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        container.getChildren().addAll(imgView, button);

        return container;
    }

    /**
     * Populate the menu based on the amount of categories in the database.
     *
     * @throws SQLException
     */
    public void populateCategoryMenu() throws SQLException {
        for (int i = 1; i <= Database.MetadataHandler.getTableCount("tblCategory"); i++) {
            Category category = new Category();
            menuVBox.getChildren().add(createCategoryMenu(category, i));
        }
    }

    /**
     * Show all the product that has been added to the basket.
     *
     * @throws SQLException
     */
    public void showCart() throws SQLException {
        JFXListView BasketListView = new JFXListView();
        BasketListView.getItems().clear();
        for (int id : basket.getProductsList()) {

            SQLServer.statement = SQLServer.connect.createStatement();

            ResultSet rs;
            try {
                rs = SQLServer.statement.executeQuery("SELECT Name,Price FROM tblProduct WHERE ProductID = " + id);

                while (rs.next()) {
                    String name = rs.getString(1);
                    String price = rs.getString(2);

                    String formattedPrice = "Pris: " + price.replace(".", ",") + " dkkr";

                    StackPane root = new StackPane();

                    HBox hbox = new HBox();
                    VBox vbox = new VBox();

                    Label label1 = new Label(name);
                    Label label2 = new Label(formattedPrice);

                    vbox.getChildren().addAll(label1, label2);
                    hbox.getChildren().addAll(vbox);
                    root.getChildren().addAll(hbox);

                    BasketListView.getItems().add(root);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Text("Current Basket"));
        content.setBody(BasketListView);

        JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button = new JFXButton("Close");
        button.setOnAction(event -> dialog.close());
        content.setActions(button);

        dialog.show();
    }

    /**
     * Load admin panel FXML
     *
     * @throws IOException
     */
    public void loadAdminFxml() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Backend.fxml"));
        Parent root = fxmlLoader.load();
        JFXDecorator decorator = new JFXDecorator(adminStage, root);
        decorator.setCustomMaximize(false);
        Scene scene = new Scene(decorator);
        adminStage.setScene(scene);
    }

    /**
     * Create admin panel button
     */
    public void addAdminButton() {
        JFXButton adminPanel = new JFXButton();
        FontAwesomeIconView glyph = new FontAwesomeIconView(FontAwesomeIcon.LOCK);
        glyph.setStyle("-fx-fill: #FF8C00");
        adminPanel.setGraphic(glyph);

        adminPanel.addEventHandler(ActionEvent.ACTION, event -> {
            adminStage.setTitle("Admin Panel");
            adminStage.show();
        });

        menuVBox.getChildren().add(adminPanel);
    }

    /**
     * Complete the purchase.
     *
     * @throws SQLException
     */
    public void makePurchase() throws SQLException {
        Label label = new Label("Your total is " + basket.calculateTotalPrice() + " dkkr");

        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Text("Thank you for your purchase!"));
        content.setBody(label);

        JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button = new JFXButton("Close");
        button.setOnAction(event -> dialog.close());
        content.setActions(button);

        dialog.show();

        basket.completeOrder(1);
        checkStock();
        basket.deleteBasket();
        basketCounter = 0;
        BasketButton.setText("Basket");
    }

    /**
     * Check stock. If the product quantity is less than the min quantity required,
     * contact the supplier to order new products.
     *
     * @throws SQLException
     */
    public void checkStock() throws SQLException {
        for (int productId : basket.getProductsList()) {
            Product product = new Product();
            product.getProductDetails(productId);

            if (product.getQuantity() < product.getMinQuantity()) {
                Supplier supplier = new Supplier();
                supplier.getSupplierInfo(supplier.getName(productId));
                supplier.orderNewProduct(productId, 25);
            } else {
                product.updateInventory(productId);
            }
        }
    }

    /**
     * Cancel the purchase and clear the basket.
     */
    public void cancelPurchase() {
        basket.deleteBasket();
        basketCounter = 0;
        BasketButton.setText("Basket");
    }


}