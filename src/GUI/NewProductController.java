package GUI;

import Database.MetadataHandler;
import Database.SQLServer;
import Functionality.Category;
import Functionality.Product;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NewProductController {

    @FXML
    private JFXTextField productNameTxf, productPriceTxf, productQuantityTxf, minQuantityTxf;
    @FXML
    private JFXComboBox<String> productCategoryCombo, productSupplierCombo;
    @FXML
    private Label imagePathLbl;
    @FXML
    private JFXCheckBox imageCheckbox;

    public void initialize() throws SQLException {
        populateCombo();
    }

    /**
     * Create a new product and insert it into the database.
     *
     * @throws SQLException
     */
    public void createProduct() throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();

        String productName = productNameTxf.getText();
        double productPrice = Double.parseDouble(productPriceTxf.getText());
        String categoryName = productCategoryCombo.getValue();
        String supplierName = productSupplierCombo.getValue();
        int quantity = Integer.parseInt(productQuantityTxf.getText());
        int minQuantity = Integer.parseInt(minQuantityTxf.getText());
        int categoryId = 0;
        int supplierId = 0;


        ResultSet rs = SQLServer.statement.executeQuery("SELECT CategoryID FROM tblCategory WHERE Name = '" + categoryName + "';");
        while (rs.next()) {
            categoryId = rs.getInt("CategoryID");
        }

        rs = SQLServer.statement.executeQuery("SELECT SupplierID FROM tblSupplier WHERE Name = '" + supplierName + "';");
        while (rs.next()) {
            supplierId = rs.getInt("SupplierID");
        }

        if (imageCheckbox.isSelected()) {
            Product.insertProduct(productName, quantity, categoryId, productPrice, supplierId, minQuantity);
        } else {
            String imagePath = imagePathLbl.getText();
            Product.insertProduct(productName, quantity, categoryId, productPrice, imagePath, supplierId, minQuantity);
        }

        BackendController.getNewProductStage().close();
    }

    /**
     * Open filechooser to add an image to the product.
     */
    public void handleAddImage() {
        FileChooser fileChooser = new FileChooser();
        imagePathLbl.setText(fileChooser.showOpenDialog(BackendController.getNewProductStage()).getPath());
    }

    /**
     * Populate the comboboxes with category and supplier names.
     *
     * @throws SQLException
     */
    public void populateCombo() throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();

        for (int i = 1; i <= MetadataHandler.getTableCount("tblCategory"); i++) {
            Category category = new Category();
            productCategoryCombo.getItems().add((category.getCategoryName(i)));
        }

        for (int i = 1; i <= MetadataHandler.getTableCount("tblSupplier"); i++) {
            ResultSet rs = SQLServer.statement.executeQuery("SELECT Name FROM tblSupplier WHERE SupplierID = " + i);
            while (rs.next()) {
                productSupplierCombo.getItems().add(rs.getString("Name"));
            }
        }
    }
}
