package GUI;

import Functionality.Supplier;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;

import java.sql.SQLException;

public class NewSupplierController {

    @FXML
    private JFXTextField supplierNameTxf, supplierPhoneTxf, supplierEmailTxf, supplierZipcodeTxf, supplierCityTxf, supplierAddressTxf;

    /**
     * Create a new supplier and insert it into the database.
     *
     * @throws SQLException
     */
    public void handleNewSupplier() throws SQLException {

        String supplierName = supplierNameTxf.getText();
        String supplierPhone = supplierPhoneTxf.getText();
        String supplierEmail = supplierEmailTxf.getText();
        String supplierZipcode = supplierZipcodeTxf.getText();
        String supplierCity = supplierCityTxf.getText();
        String supplierAddress = supplierAddressTxf.getText();
        String supplierFullAddress = supplierAddress + ", " + supplierZipcode + " " + supplierCity;

        Supplier supplier = new Supplier();
        supplier.insertSupplier(supplierName, supplierPhone, supplierEmail, supplierFullAddress);
        BackendController.getNewSupplierStage().close();
    }
}
