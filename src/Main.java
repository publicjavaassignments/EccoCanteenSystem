import Database.SQLServer;
import com.jfoenix.controls.JFXDecorator;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.SQLException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("GUI/Frontend.fxml"));
        Parent root = loader.load();
        JFXDecorator decorator = new JFXDecorator(stage, root);
        decorator.setCustomMaximize(false);
        Scene scene = new Scene(decorator);
        decorator.setCustomMaximize(true);
        stage.setScene(scene);
        scene.getStylesheets().add(getClass().getResource("GUI/Theme.css").toExternalForm());
        stage.setTitle("Canteen");
        stage.show();
    }

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        SQLServer.connect();
        launch(args);
    }
}
